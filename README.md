<!--
SPDX-FileCopyrightText: 2020-2024 Mark Jonas <toertel@gmail.com>

SPDX-License-Identifier: MIT
-->

[![Pipeline status](https://gitlab.com/toertel/meta-doom/badges/kirkstone/pipeline.svg)](https://gitlab.com/toertel/meta-doom/pipelines)
[![REUSE status](https://api.reuse.software/badge/gitlab.com/toertel/meta-doom)](https://api.reuse.software/info/gitlab.com/toertel/meta-doom)

# meta-doom

This meta-layer supplies recipes for compiling Doom from id Software with The Yocto Project.

The meta-layer is has been tested on Poky's core-image-minimal using qemux86-64 with The Yocto Project 4.0 "Kirkstone".

> :warning: **WARNING**
> Unfortunately Ubuntu 24.04 uses an apparmor configuration which is not
> compatible with bitbake. See [Ubuntu Launchpad bug #2056555](https://bugs.launchpad.net/ubuntu/+source/apparmor/+bug/2056555)
> and [Workaround for uid_map error on Ubuntu 24.04](https://lists.yoctoproject.org/g/yocto/topic/106192359#)
> for details. If you know what you are doing, you can run `echo 0 | sudo tee /proc/sys/kernel/apparmor_restrict_unprivileged_userns`
> to work around the problem.

## Setup

For setting up the build and documenting how to build different configurations [kas](https://github.com/siemens/kas) is used. Please read and follow the [kas dependencies & installation](https://kas.readthedocs.io/en/latest/userguide.html#dependencies-installation) for installing kas.

## Build

Create a directory `yocto` (or any other name of your choice) and clone meta-doom into it.

```
mkdir yocto
cd yocto
git clone https://gitlab.com/toertel/meta-doom.git
```

After cloning you can switch to a different branch in case you do not want to build the default version.

```
cd meta-doom
git checkout kirkstone
cd ..
```

### DirectFB (framebuffer)

Build an image with Doom using DirectFB.

```
kas build meta-doom/kas/doom-directfb-kirkstone.yaml
```

### X11

Build an image with Doom using X11.

```
kas build meta-doom/kas/doom-x11-kirkstone.yaml
```

## Run

### DirectFB (framebuffer)

> :warning: **WARNING**
> On Ubuntu 24.04 running QEMU with an SDL or GTK+ frontend is at least tricky.
> Here, graphics is shown using the `publicvnc` option of *runqemu*. You can
> connect to the VNC server on 127.0.0.1:5900.

Start QEMU with the image previously built.

```
kas shell -c "runqemu core-image-minimal kvm slirp serial publicvnc" meta-doom/kas/doom-directfb-kirkstone.yaml
```

Login as user _root_ and and start `chocolate-doom`.

```
chocolate-doom
```

### X11

> :warning: **WARNING**
> How to run QEMU on Ubuntu 24.04 with an SDL or GTK+ frontend is currently
> unknown. Thus, the X11 example below does not work as it uses and needs
> the `gl sdl` feature set.

Start QEMU with the image previously built.

```
kas shell -c "runqemu core-image-minimal kvm slirp serial gl sdl" meta-doom/kas/doom-x11-kirkstone.yaml
```

Start `chocolate-doom` in the X11 terminal.

```
DISPLAY=:0 chocolate-doom
```

**Note:** In starting of QEMU fails because _package dri was not found_ you need to instal the MESA development package of your distribution. For Ubuntu `sudo apt install mesa-common-dev` does the trick.

## Known Issues

The following are known issues. If you have an idea to fix them, please send patches! :)

### Generic

#### Audio output does not work

Audio output is not working (yet).

If you know how to do it, please send a patch.
